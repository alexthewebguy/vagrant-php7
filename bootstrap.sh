#!/usr/bin/env bash

Update () {
    echo "-- Update packages --"
    sudo apt-get update
    sudo apt-get upgrade
}

Update
1
echo "-- Prepare configuration for MySQL --"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password root"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password root"

sudo mkdir /vagrant/projects

echo "-- Install tools and helpers --"
sudo apt-get install -y vim python-software-properties htop curl git npm
Update

echo "-- Install PPA's --"
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:ondrej/php-7.0
sudo LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
sudo add-apt-repository ppa:chris-lea/node.js
sudo add-apt-repository ppa:rwky/redis
Update
sudo apt-get update

echo "-- Install Main packages --"
sudo apt-get install -y php7.0-common php7.0-json php7.0-opcache php7.0-cli libapache2-mod-php7.0 php7.0 php7.0-mysql php7.0-fpm php7.0-curl php7.0-gd php7.0-dev php7.0-mbstring php7.0-mcrypt php7.0-odbc php7.0-xml php7.0-zip
sudo apt-get install -y apache2 mysql-server mysql-client git-core nodejs rabbitmq-server redis-server
sudo apt-get install ntpdate
sudo ntpdate time.windows.com
Update

echo "-- Install XDebug --"
sudo wget http://xdebug.org/files/xdebug-2.4.1.tgz
sudo tar -xvzf xdebug-2.4.1.tgz
cd xdebug-2.4.1
sudo phpize
sudo ./configure
sudo make
sudo cp modules/xdebug.so /usr/lib/php/20151012
sudo sed -i "$ a\zend_extension = /usr/lib/php/20151012/xdebug.so" /etc/php/7.0/apache2/php.ini
sudo sed -i "$ a\xdebug.remote_enable=1" /etc/php/7.0/apache2/php.ini
sudo sed -i "$ a\xdebug.remote_host=10.1.10.10" /etc/php/7.0/apache2/php.ini
sudo sed -i "$ a\xdebug.remote_port=9000" /etc/php/7.0/apache2/php.ini
sudo sed -i "$ a\xdebug.remote_connect_back=on" /etc/php/7.0/apache2/php.ini
sudo sed -i "$ a\xdebug.idekey='vagrant'" /etc/php/7.0/apache2/php.ini

echo "-- Install Ruby & Dependencies --"
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev11 zip unzip zip

cd /var/tmp/
sudo wget http://ftp.ruby-lang.org/pub/ruby/2.3/ruby-2.3.1.tar.gz
sudo tar -xzvf ruby-2.3.1.tar.gz
cd ruby-2.3.1/
sudo ./configure
sudo make
sudo make install
sudo ruby -v
sudo gem install bundler

sudo curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo gem install rails -v 4.2.6

echo "-- Configure PHP &Apache --"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php/7.0/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php/7.0/apache2/php.ini
sudo a2enmod rewrite

echo "-- Creating virtual hosts --"
cat << EOF | sudo tee -a /etc/apache2/sites-available/phpmyadmin.conf
<VirtualHost *:80>
    DocumentRoot /var/projects/phpmyadmin
    ServerName phpmyadmin.dev
    ServerAlias www.phpmyadmin.dev

    ErrorLog /var/projects/phpmyadmin/logs/error_log
    CustomLog /var/projects/phpmyadmin/logs/access_log common
    CustomLog /var/projects/phpmyadmin/logs/referer_log referer
    CustomLog /var/projects/phpmyadmin/logs/combined_log combined

    <Directory "/var/projects/phpmyadmin">
        Require all granted
    </Directory>
</VirtualHost>
EOF
sudo ln -fs /vagrant/projects /var/projects
sudo mkdir -p /var/projects/{startproject,startproject/logs,phpmyadmin,phpmyadmin/logs}

sudo a2ensite phpmyadmin.conf

echo "-- Restart Apache --"
sudo /etc/init.d/apache2 restart

echo "-- Install Composer --"
curl -s https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
sudo chmod +x /usr/local/bin/composer

echo "-- Setup StartProject --"
sudo git clone https://alexthewebguy@bitbucket.org/alexthewebguy/startproject.git /var/projects/startproject
sudo ln -s /var/projects/startproject/apache_startproject.conf /var/apache2/sites-available/startproject.conf
sudo a2ensite startproject

echo "-- Install phpMyAdmin --"
wget -k https://files.phpmyadmin.net/phpMyAdmin/4.0.10.11/phpMyAdmin-4.0.10.11-english.tar.gz
sudo tar -xzvf phpMyAdmin-4.0.10.11-english.tar.gz -C /var/projects/
sudo rm phpMyAdmin-4.0.10.11-english.tar.gz
sudo mv /var/projects/phpMyAdmin-4.0.10.11-english/* /var/projects/phpmyadmin

echo "-- Configure GIT --"
git config --global color.ui true
echo "Enter GIT UsernName: "
read gitusername
git config --global user.name $gitusername
echo "Enter GIT Email: "
read gitemail
git config --global user.email $gitemail
ssh-keygen -t rsa -b 4096 -C $gitemail

echo "-- Install Compass --"
sudo gem update --system
sudo gem install compass

echo "-- Finishing Touches --"
echo "You will need to add the vagrant servers SSH key onto your GitHub account to allow vagrant to access GIT."
echo "You can find the key by running this command:"
echo "cat ~/.ssh/id_rsa.pub"

